FROM nginx:latest

COPY /config/default.conf /etc/nginx/conf.d/
COPY /custom_page/index.html /usr/share/nginx/html

# It's gonna be overidden by publish, but just in case
EXPOSE 9090